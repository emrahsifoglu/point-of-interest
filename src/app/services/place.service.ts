import { Injectable } from '@angular/core';
import { Place } from '../models/place';

@Injectable()
export class PlaceService {

  private places: Place[];
  private nextId: number;

  constructor() {
    let places = this.getPlaces();
      
    if(places.length == 0) {
        this.nextId = 0 
    } else {
        let nextId = places[places.length - 1].id;
        this.nextId = nextId;
    }
  }

  public addPlace(place: Place): void {
    let places = this.getPlaces();

    place.id = this.nextId;
    places.push(place);

    this.setLocalStoragePlaces(places);
    this.nextId++;
  }

  public updatePlace(place: Place) {
    let places = this.getPlaces();

    places.forEach(function (currentPlace: Place, index) {
      if (currentPlace.id == place.id) {
        places[index] = place;
      }
    });

    this.setLocalStoragePlaces(places);
  }

  public getPlaces(): Place[] {
    let localStorageItem = JSON.parse(localStorage.getItem('places'));

    return localStorageItem == null ? [] : localStorageItem.places;
  }

  public removePlace(id: number): void {
    let places = this.getPlaces();

    places = places.filter((place: Place) => place.id != id);

    this.setLocalStoragePlaces(places);
  }

  private setLocalStoragePlaces(places: Place[]) {
    localStorage.setItem('places', JSON.stringify({places: places}));
  }

}
