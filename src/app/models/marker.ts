export class Marker {

    public label: string;
    public point: {
      lat: number;
      lng: number;
    }
    public info: string;
  
    constructor(
      label: string, 
      point: {
        lat: number;
        lng: number;
      },
      info: string
    ) {
      this.label = label;
      this.point = point;
      this.info = info;
    }
  }
  