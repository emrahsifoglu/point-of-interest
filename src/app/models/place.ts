import { Marker } from "./marker";

export class Place {
  public id: number;
  public marker: Marker;

  constructor(
    marker: Marker
  ) {
    this.marker = marker;
  }
}
