﻿import { Component } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { PlaceService } from './services/place.service';
import { Place } from './models/place';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {

  constructor(
    private placeService: PlaceService
  ) {

  }

}
