import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Marker } from './../../models/marker';
import { Place } from '../../models/place';
import { PlaceService } from '../../services/place.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'place-item',
  templateUrl: './place-item.component.html',
  styleUrls: ['./place-item.component.css']
})
export class PlaceItemComponent implements OnInit {

  @Input()
  private place: Place;
  private placeForm: FormGroup;

  modalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private placeService: PlaceService
  ) { 

  }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.placeForm = new FormGroup({
        id: new FormControl(''),
        name: new FormControl(''),
        description: new FormControl(''),
        lat: new FormControl(''),
        lng: new FormControl(''),
    });
  }

  public showEditPlaceForm(template: TemplateRef<any>) {
    this.createForm();

    this.placeForm.setValue({
        id: this.place.id,
        name: this.place.marker.label,
        description: this.place.marker.info,
        lat: this.place.marker.point.lat,
        lng: this.place.marker.point.lng,
    });

    this.modalRef = this.modalService.show(template);
  }

  private removePlace(): void {
    this.placeService.removePlace(this.place.id);
  }

  /**
   * @param {NgForm} placeForm
   */
  public onSubmitPlaceForm(placeForm: NgForm) {
    let marker = new Marker(placeForm.value.name, {
      lat: placeForm.value.lat,
      lng: placeForm.value.lng
    },
    
    placeForm.value.description); 

    let place = new Place(marker);
    place.id = placeForm.value.id;

    this.placeService.updatePlace(place);
    this.closeFirstModal();
  }

  /**
   * @param {TemplateRef<any>} template 
   */
  public showMap(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  public closeFirstModal() {
    this.modalRef.hide();
    this.modalRef = null;
  }

}
