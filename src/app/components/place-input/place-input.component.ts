import { Component, OnInit } from '@angular/core';
import { PlaceService } from '../../services/place.service';
import { Marker } from './../../models/marker';
import { Place } from '../../models/place';

@Component({
  selector: 'place-input',
  templateUrl: './place-input.component.html',
  styleUrls: ['./place-input.component.css']
})
export class PlaceInputComponent implements OnInit {

  public name: string;
  public lat: number;
  public lng: number;
  public description: string;

  constructor(private placeService: PlaceService) {
    this.clearPlaceProperties();
  }

  ngOnInit() {
  }

  private addPlace(): void {
    let marker = new Marker(this.name, {
      lat: this.lat,
      lng: this.lng
    },
    this.description); 
    let place = new Place(marker);
    this.placeService.addPlace(place);
    this.clearPlaceProperties();
  }

  private clearPlaceProperties() {
    this.name = '';
    this.lat = null;
    this.lng = null;
    this.description = '';
  }
}
