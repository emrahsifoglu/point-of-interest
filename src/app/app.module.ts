﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { HeaderComponent } from './components/header/header.component';
import { PlaceInputComponent } from './components/place-input/place-input.component';
import { PlaceService } from './services/place.service';
import { PlaceItemComponent } from './components/place-item/place-item.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'API_KEY',
            libraries: ["places"],
        }),
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        PlaceInputComponent,
        PlaceItemComponent,
    ],
    providers: [
        PlaceService
    ],
    exports: [ModalModule],
    bootstrap: [AppComponent]
})

export class AppModule { }
